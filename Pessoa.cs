﻿using Newtonsoft.Json;

namespace ForVsForEach_Performance
{
    public class Pessoa
    {
        public int Id { get; set; }
        [JsonProperty("NomeInteiro")]
        public string Nome { get; set; }

        public Pessoa(int id)
        {
            Id = id;
        }

        public Pessoa(int id, string nome)
        {
            Id=id;
            Nome=nome;
        }

        public (int id, string nome, int idade) RetornarPessoaIdade()
        {
            return (this.Id, this.Nome, 24);
        }

        public (int id, string nome, Pessoa idade) RetornarPessoaIdadee()
        {
            return (this.Id, this.Nome, null);
        }

        public Tuple<int, string> ToTuple() {
            return Tuple.Create(1,"teste");
        }
    }

    public class Teste
    {
        public bool Like { get; private set; } = true;
    }
}
