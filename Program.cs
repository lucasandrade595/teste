﻿using ForVsForEach_Performance;

var teste1 = new Teste();

Console.WriteLine(teste1.Like);



Console.WriteLine();
var pessoa = new Pessoa(1, "Lucas");

var types = pessoa.GetType();

var property = (types.GetProperties());
foreach (var propertyInfo in property)
{
    //if (propertyInfo.CustomAttributes.Any())
    //{
    var customAttributes = propertyInfo.CustomAttributes.Any(p => p.AttributeType.Name.Equals("JsonPropertyAttribute"));
    Console.WriteLine(customAttributes);
    Console.WriteLine(propertyInfo.CustomAttributes.FirstOrDefault());
    //}
}

#region metodo que retonar duas entradas nomeadas
var teste = pessoa.RetornarPessoaIdade();
Console.WriteLine($"Id: {teste.id}");
Console.WriteLine($"Idade: {teste.idade}");
Console.WriteLine($"Nome: {teste.nome}");
#endregion

//var pessoas = new List<Pessoa>();

//for(var count = 0 ; count < 100000; count++)
//{
//    pessoas.Add(new Pessoa(count));
//}

//var arrayPessoas = pessoas.ToArray();

////var watchFor = new Stopwatch();
//var watchForEach = new Stopwatch();

//watchFor.Start();
//for(var count = 0 ; count < pessoas.Count; count++)
//{

//}
//watchFor.Stop();

//watchForEach.Start();
//foreach(var pessoa in pessoas)
//{

//}
//watchForEach.Stop();

//Console.WriteLine($"For Array: {watchFor.Elapsed.TotalMilliseconds}", Console.ForegroundColor = ConsoleColor.DarkRed);
//Console.WriteLine($"ForEach Array: {watchForEach.Elapsed.TotalMilliseconds}", Console.ForegroundColor = ConsoleColor.DarkGreen);
//Console.ForegroundColor = ConsoleColor.White;


// Para 100K For em um Array leva 3.5 milesegundos
// Para 100k Foreach em um Array leva 0,250 microsegundos

// Para 100K For em uma List leva 0,200  microsegundos
// Para 100K Foreach em uma List leva 0,600  microsegundos
//---------------------------------------------------------------------
//(Pessoa, int) RetornarPessoaIdade() => (new Pessoa(1, "t"), 1);
(Pessoa, int) RetornarPessoaIdade()
{
    return (new Pessoa(1, "t"), 1);
}
